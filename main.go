package main

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"sync"
)

func setSsnId(w http.ResponseWriter) string {
	kuki := []byte("ssnid=01234567891123456789212345678931; SameSite=Lax; HttpOnly")
	//              012345678911234567892123456789312345678
	rand.Read(kuki[14:38])
	base64.URLEncoding.Encode(kuki[6:38], kuki[14:38])
	println("new id:", string(kuki[6:38]))
	w.Header()["Set-Cookie"] = []string{string(kuki)}
	return string(kuki[6:38])
}
func getSsnId(r *http.Request) (id string, err error) {
	_kuki := r.Header["Cookie"]
	if len(_kuki) == 0 {
		err = errors.New("1.Sess ID Err")
		return
	}
	kuki := _kuki[0]
	kukiLen := len(kuki)
	if kukiLen == 0 {
		err = errors.New("2.Sess ID Err")
		return
	}
	i := 6
	for i < kukiLen && string(kuki[i-6:i]) != "ssnid=" {
		i++
	}
	if (i + 32) <= kukiLen {
		id = kuki[i : i+32]
	} else {
		err = errors.New("3.Sess ID Err")
	}
	return
}

/*
func delSsnId(w http.ResponseWriter) {
	w.Header()["Set-Cookie"] = []string{"ssnid=; Expires=Sat, 01 Jan 2000 00:00:00 GMT"}
}
*/
func broker() (mssg chan string, reg func(string) chan string, unreg func(string)) {
	mssgs := map[string]chan string{}
	mssg = make(chan string, 1)
	var mtx sync.Mutex
	go func() {
		for {
			_mssg := <-mssg
			for _, __mssg := range mssgs {
				__mssg <- _mssg
			}
		}
	}()
	reg = func(id string) (mssg chan string) {
		mssg = make(chan string, 1)
		mtx.Lock()
		mssgs[id] = mssg
		mtx.Unlock()
		println("reg:", id)
		return
	}
	unreg = func(id string) {
		mtx.Lock()
		delete(mssgs, id)
		mtx.Unlock()
		println("unreg:", id)
	}
	return
}
func index(file *os.File) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		io.Copy(w, file)
		file.Seek(0, 0)
	}
}
func sse(reg func(string) chan string, unreg func(string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, _ := getSsnId(r)
		if id == "" {
			id = setSsnId(w)
		}
		w.Header()["Content-Type"] = []string{"text/event-stream"}
		flusher, _ := w.(http.Flusher)
		flusher.Flush()
		mssg := reg(id)
		for {
			select {
			case <-r.Context().Done():
				unreg(id)
				return
			case str := <-mssg:
				println("id:", id, " mssg:", str)
				fmt.Fprintf(w, "data: %s\n\n", str)
				flusher.Flush()
			}
		}
	}
}
func message(mssg chan string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		mssg <- r.PostFormValue("mssg")
	}
}

func main() {
	file, err := os.Open("client/index.html")
	if err != nil {
		println(err.Error())
		return
	}
	defer file.Close()
	http.HandleFunc("/", index(file))
	mssg, reg, unreg := broker()
	http.HandleFunc("/sse", sse(reg, unreg))
	http.HandleFunc("/message", message(mssg))
	http.ListenAndServe(":8080", nil)
}
